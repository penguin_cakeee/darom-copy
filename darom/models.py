from django.db import models


class Lot(models.Model):
    author = models.ForeignKey(
        "auth.User", on_delete=models.CASCADE, verbose_name="Author", blank=True, null=True
    )
    title = models.CharField(max_length=80)
    text = models.TextField()
    publish_date = models.DateTimeField(auto_now_add=True)
    prise = models.CharField(max_length=80)

    def __str__(self):
        return f'{self.title}'

