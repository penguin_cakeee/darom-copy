const swiper = new Swiper('.swiper-container', {
  speed: 400,
  mousewheel: true,
  pagination: {
    el: '.swiper-pagination',
    type: 'bullets',
    dynamicBullets: true,
  },
});
