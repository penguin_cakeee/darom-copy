from django.urls import path
from darom.views import lot_list, lot_detail, lot_new

urlpatterns = [

    path('', lot_list, name='lot_list'),
    path('detail/<int:lot_pk>/', lot_detail, name='lot_detail'),
    path('new/', lot_new, name='lot_new')
]
