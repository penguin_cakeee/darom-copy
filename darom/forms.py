from django import forms
from darom.models import Lot


class LotForm(forms.ModelForm):
    class Meta:
        model = Lot
        fields = [
            'title',
            'text',
            'prise'
        ]
