from django.apps import AppConfig


class DaromConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'darom'
