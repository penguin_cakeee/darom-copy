from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.utils import timezone
from darom.models import Lot
from darom.forms import LotForm


def lot_list(request):
    lots = Lot.objects.all()
    return render(request, 'darom/lot_list.html', {'lots': lots})


def lot_detail(request, lot_pk):
    lot = Lot.objects.filter(pk=lot_pk).first()
    return render(request, 'darom/lot_detail.html', {'lot': lot})


def lot_new(request):
    form = LotForm()
    if request.method == 'GET':
        return render(request, 'darom/lot_new.html', {'form': form})
    if request.method == 'POST':
        form = LotForm(request.POST)
        if form.is_valid():
            lot = form.save(commit=False)
            lot.publish_date = timezone.now()
            lot.save()
            return redirect('lot_detail', lot_pk=lot.pk)


def lot_new(request):
    form = LotForm()
    if request.method == 'GET':
        return render(request, 'darom/lot_new.html', {'form': form})
    if request.method == 'POST':
        form = LotForm(request.POST)
        if form.is_valid():
            lot = form.save(commit=False)
            lot.publish_date = timezone.now()
            lot.save()
            return redirect('lot_detail', lot_pk=lot.pk)
